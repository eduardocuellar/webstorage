function guardarEnSessionStorage() {
  var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
  var txtValor = document.getElementById("txtValor"); /* Referencia al input de valor */
  var clave = txtClave.value;
  var valor = txtValor.value;
  sessionStorage.setItem(clave, valor);
  var objeto = {
      nombre:"Eduardo",
      apellidos:"Cuellar Amancay",
      ciudad:"Lima",
      pais:"Perú"
    };
    localStorage.setItem("json", JSON.stringify(objeto));
  }

  function leerDeSessionStorage() {
    var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
    var clave = txtClave.value;
    var valor = sessionStorage.getItem(clave);
    var spanValor = document.getElementById("spanValor");
    spanValor.innerText = valor;
    var datosUsuario = JSON.parse(localStorage.getItem("json"));
  console.log(datosUsuario.nombre);
  console.log(datosUsuario.pais);
  console.log(datosUsuario);
}

function limpiarDeSessionStorage() {
  sessionStorage.clear();
  var spanValor = document.getElementById("spanValor");
  spanValor.innerText = "Clear";
}

function tamanoDeSessionStorage() {
  var tamano = sessionStorage.length;
  var spanValor = document.getElementById("spanValor");
  spanValor.innerText = tamano;
}

function eliminarDeSessionStorage() {
  var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
  var clave = txtClave.value;
  sessionStorage.removeItem(clave);
  var spanValor = document.getElementById("spanValor");
  spanValor.innerText = "Eliminado";
}
